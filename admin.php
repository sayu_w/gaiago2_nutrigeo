<!doctype html>
<html lang="fr">

  <head>

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Oxygen&display=swap" rel="stylesheet">

    <!-- style css -->
    <link rel="stylesheet" href="css/main.css">

    <!-- Bootstrap CSS -->
    
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    

</head>

<body>
    <!-- Partie Header -->
<header>
    <nav class="navbar fixed-top navbar-light bg-light">
    <!-- logo -->
    <img src="image/logo_gaiago_small.png" alt="logo de Gaiago">
    <!-- image produit -->
    <a class="navbar-brand" href="index.html"><img src="image/home_logo_admin" alt="home">Home</a>
    <img src="image/freeN100_80.png" alt="freeN100_2">
    
    <!-- <form class="form-inline">
        <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
    </form> -->
    </nav>
    <!-- image header -->
    
</header>

<!-- <section class="centered">
    
    <p>Merci de vous connecter</p>
    <form action="login.php" method="POST">
        <input type="text" name="nom" placeholder="Nom d'utilsateur">
        <input type="password" name="password" placeholder="Mot de passe">
        <br>
        <input type="submit" value="Envoyer">
    </form>
</section> -->

<form class="center_admin" action="login.php" method="POST">
<h1>Administration</h1>
  <div class="form-group">
    <label for="exampleInputEmail1">Nom d'utilisateur</label>
    <input type="text" name="nom" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
    <small id="emailHelp" class="form-text text-muted">Veuillez entrez votre nom d'utilisateur</small>
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Password</label>
    <input type="password" name="password" class="form-control" id="exampleInputPassword1">
  </div>
  <button id="btn-admin" type="submit" class="btn form-control form-group" style="background-color: #d16b18;">Envoyer</button>
</form>

    <!-- Partie Footer -->
    <div class="sm_footer"><small class="sm_hidden">...</small></div>

<footer id="footer">
    

    <div class="container">
      <div class="row">
        <div class="col-sm">

          <p>&copy; Gaiago</p>
      
          <a href="https://twitter.com/gaiago_agri" class="color-2"><img class="img-footer" src="image/twitter.svg" alt="twitter"></a>
          <a href="https://www.linkedin.com/company/gaiago/" class="color-2"><img class="img-footer" src="image/linkedin.svg" alt="twitter"></a>
          <a href="https://www.facebook.com/gaiago.eu/?ref=bookmarks" class="color-2"><img class="img-footer" src="image/facebook.svg" alt="twitter"></a>
    
        </div>
        <div class="col-sm">

          <p>CONTACT</p>
          <img src="image/shapes-and-symbols32px.png" alt="phone"><a href="tel:+33299887391">  02 99 88 73 91</a><br>
          <a href="mailto:serviceclient@gaiago.eu">Nous envoyez un Mail</a><br>
          <small><i>serviceclient@gaiago.eu</i></small>
          <i class="fas fa-phone-volume"></i>
        </div>
        <div class="col-sm">

          <p>Solutions</p>
          <a href="https://gaiago.eu/solutions">Nos Produits</a>
         

        </div>
        <div class="col-sm">
           <p>Mentions légales</p>
          <a href="https://gaiago.eu/legal">Mentions et RGPD</a>
          
        </div>
        
      </div>
    </div>

  </footer>

    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
  
</body>
</html>